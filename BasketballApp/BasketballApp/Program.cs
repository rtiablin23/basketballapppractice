﻿using BasketballApp.Enums;
using BasketballApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace BasketballApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                Seed(db);
                var teams = db.Teams
                    .Include(p => p.Players);
                foreach (var b in teams)
                {
                    Console.WriteLine("---------------------------");
                    Console.WriteLine($"Name: {b.Name}");
                    Console.WriteLine($"City: {b.City}");
                    Console.WriteLine($"Conference: {b.Conference}");
                    Console.WriteLine($"Fund Year: {b.FundYear}");
                    Console.WriteLine($"Wins: {b.WinCountPerSeas}");
                    Console.WriteLine($"Loss: {b.LossCountPerSeas}");
                    Console.WriteLine("---------------------------");

                    foreach (var c in b.Players)
                    {
                        Console.WriteLine("---------------------------");
                        Console.WriteLine($"Name: {c.Name}");
                        Console.WriteLine($"Surname: {c.Surname}");
                        Console.WriteLine($"Height: {c.Height}");
                        Console.WriteLine($"Weigth: {c.Weigth}");
                        Console.WriteLine($"ShirtNumber: {c.ShirtNumber}");
                        Console.WriteLine($"Position: {c.Position}");
                        Console.WriteLine($"BirthDate: {c.BirthDate}");
                        Console.WriteLine("---------------------------");
                    }
                }
            }
        }
        public static void Seed(ApplicationDbContext db)
        {
            if (!db.Persons.Any())
            {
                var player1 = new Player
                {
                    Name = "Lebron",
                    Surname = "James",
                    Height = 2.04,
                    Weigth = 114,
                    ShirtNumber = 6,
                    Position = Position.SF,
                    BirthDate = new DateTime(1985, 12, 31),
                };

                var player2 = new Player
                {
                    Name = "Antony",
                    Surname = "Davis",
                    Height = 2.10,
                    Weigth = 121,
                    ShirtNumber = 23,
                    Position = Position.PF,
                    BirthDate = new DateTime(1987, 4, 26),
                };

                var coach = new Coach
                {
                    Name = "Frank",
                    Surname = "Vogel",
                    BirthDate = new DateTime(1973, 4, 15),
                    WorkYearCount = 12,
                };

                db.Players.Add(player1);
                db.Players.Add(player2);
                db.Coaches.Add(coach);
                db.SaveChanges();
            }

            if (!db.Teams.Any())
            {
                var team1 = new Team
                {
                    Name = "Laker",
                    City = "LosAngeles",
                    Conference = Conference.East,
                    WinCountPerSeas = 50,
                    LossCountPerSeas = 2,
                    FundYear = 1900,
                };

                db.Teams.Add(team1);
                db.SaveChanges();

            }
        }
    }
}
