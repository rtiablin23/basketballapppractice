﻿using BasketballApp.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketballApp.Models
{
    public class Coach : Person
    {

        [Required]
        public int WorkYearCount { get; set; }
        
    }
}
