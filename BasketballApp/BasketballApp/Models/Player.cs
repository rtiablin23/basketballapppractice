﻿using BasketballApp.Enums;
using BasketballApp.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketballApp.Models
{
    public class Player : Person
    {
        [ForeignKey("Team")]
        public int TeamID { get; set; }

        public double Height { get; set; }
        public double Weigth { get; set; }

        [Required]
        public int ShirtNumber { get; set; }

        [Required]
        public Position Position { get; set; }

        public virtual Team Team { get; set; }
    }
}
