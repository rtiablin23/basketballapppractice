﻿using BasketballApp.Enums;
using BasketballApp.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketballApp.Models
{
    public class Team : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("Coach")]
        public int CoachId { get; set; }

        [Required]
        public string Name { get; set; }
        public string City { get; set; }
        public Conference Conference { get; set; }
        public int? FundYear { get; set; }
        public int WinCountPerSeas { get; set; }
        public int LossCountPerSeas { get; set; }
        public virtual ICollection<Player> Players { get; set; }

        public Coach Coach { get; set; }
    }
}