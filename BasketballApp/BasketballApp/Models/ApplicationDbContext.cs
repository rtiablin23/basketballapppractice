﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasketballApp.Models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Person> Persons { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"C:\Users\User\Desktop\Project\GitRepositories\BasketballAppPractice\BasketballApp\BasketballApp\");
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();

            var connectionString = config.GetConnectionString("DefaultConnection");

            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Team>()
                .HasIndex(x => x.Name)
                .IsUnique();

            modelBuilder
                .Entity<Player>()
                .HasIndex(x => x.Name)
                .IsUnique();

            modelBuilder
                .Entity<Player>()
                .HasIndex(x => x.Surname)
                .IsUnique();

            modelBuilder
                .Entity<Coach>()
                .HasIndex(x => x.Name)
                .IsUnique();

            modelBuilder
                .Entity<Coach>()
                .HasIndex(x => x.Surname)
                .IsUnique();
        }
    }
}
